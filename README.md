# Cloud Run Demo

Demo for Continuous Deployment to Cloud Run with Cloud Source Repositories and Cloud Build.
For this demo we recommend to use the Google Cloud Shell. It provides all tools you need out-of-the-box for this demo.

## Exercise

Build a pipeline to deploy a Cloud Run service from this repository.
All needed files are provided in this repository.

## Steps - Overview

1. Enable needed APIs
1. Grant the respective permissions
1. Create a Cloud Repository in your project and add this repository content to it
1. Create a trigger in Cloud Build
1. Trigger build manually to verify build process works properly
1. Verify that your container image was created in Cloud Registry
1. Verify that your service was created in Cloud Run
1. Make a change to the html-code in main.go
1. Push changes to your Cloud Repository
1. Verify updates in Cloud Repository, Cloud Build, Cloud Registry and Cloud Run
1. Generate load on your website
1. Verify the impact on your Cloud Run service via metrics
1. Cleanup your Cloud Run service

### Enable APIs

#### Enable Cloud Build API

Go over the Google Cloud Console menu to **APIs & Services**. Search for **Cloud Build API** and verify that it is enabled. If it is not present, select **Library** in the left pane and search teh API and enable it.

#### Enable Cloud Run API

Open a Cloud Shell and enable the API with the following command:

```bash
gcloud services enable run.googleapis.com
```

### Grant Permissions

Grant `LONG_NUMBER@cloudbuild.gserviceaccount.com` the role: roles/run.admin

In your project, select **IAM & admin** menu. Search for an account in the form: <LONG_NUMBER>@cloudbuild.gserviceaccount.com
Click edit on the right side, **+ ADD ANOTHER ROLE** and add the role **Cloud Run Admin** to the account. You can find it, if you filter for it or go to **Cloud Run** in the left pane and then select the accurate role on the right.

Alternatively use following command:

```bash
gcloud projects add-iam-policy-binding my-project-123 --member serviceAccount:<LONG_NUMBER>@cloudbuild.gserviceaccount.com --role roles/iam.serviceAccountTokenCreator
```

Add user account LONG_NUMBER@cloudbuild.gserviceaccount.com to service account LONG_NUMBER-compute@developer.gserviceaccount.com. To do that go to Service accounts in the left menu pane, **select** the `LONG_NUMBER-compute@developer.gserviceaccount.com` service account and add `LONG_NUMBER@cloudbuild.gserviceaccount.com` as a member and add role: Service Account User.

### Create Cloud Repository

On the Cloud Console, go to Source Repositories. Select your project if desired and click 'Add Repository'.
Select 'Create new repository' and hit Continue. Provide a repository name, select your project if necessary and click 'Create'

### Create Cloud Build trigger

In menu Cloud Build: Select `Triggers` in the left menu pane. Create a trigger with the button `Add trigger`.
Provide a name for the trigger, select **Trigger type** branch and provide branch name. At least select **Cloud Build configuration file** and provide name of your Cloud Build file (e.g. cloudbuild.yaml). Save trigger.

### Trigger Cloud Build

On Google Cloud Console go to Cloud Build, select 'Triggers' tab on the left pane and click `Run trigger`.

### Verify

Now verify your build:

1. Click on History in the left pane
1. Identify your build
1. Select your build and look at the details and verify that everything worked
1. Go to Cloud Registry and select your build and verify that your image was build.
1. Go to Cloud Run and verify your service was deployed and is up and running.

### Make changes

Open the file `main.go` and make a change in the html code. Commit your changes and push your changes to your repository.

```bash
vi main.go # press i to edit file, then press ESC and enter :w! and :q

git add main.go
git commit -m "Made a change in the html code"
git push google
```

### Generate load

Open Cloud Shell or create a VM instance and execute the following command. When you copy the following then include the curly brackets. **Don't forget to change URL!!!**

```bash
{
    sudo apt update
    sudo apt install apache2-utils
    for i in 1 2 3
    do
        ab -n 100000 -c 100 -s 30 <YOUR-URL-HERE>
    done
}
```

While this little script is been executed on the Cloud Shell, go to the next step and verify the load.

### Verify metrics

Go to Cloud Run and select your deployed service. Within the metrics tab you can get an overview of the recent load on your service.

### Cleanup

Go to Cloud Run and select your service. Then delete it.
Next we delete the second service via the CLoud Shell with the following command:

```bash
gcloud beta run services delete <YOUR-SERVICE-NAME>
```
